# frozen_string_literal: true

require 'spec_helper'

describe 'comment on an epic' do
  include_context 'with integration context'

  let(:source) { 'groups' }
  let(:source_id) { group_id }

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/groups/#{group_id}/epics",
      query: { per_page: 100, state: 'opened' },
      headers: { 'PRIVATE-TOKEN' => token }) do
      epics
    end
  end

  it 'comments on the epic' do
    rule = <<~YAML
      resource_rules:
        epics:
          rules:
            - name: Rule name
              conditions:
                state: opened
              actions:
                comment: |
                  This is my comment for \#{resource[:type]}
    YAML

    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/groups/#{group_id}/epics/#{epic[:id]}/notes",
      body: { body: 'This is my comment for epics' },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end
end

describe 'triaging epic at the project level' do
  include_context 'with integration context'

  let(:source) { 'projects' }
  let(:source_id) { group_id }

  it 'shows an error and does not triage the epics' do
    rule = <<~YAML
      resource_rules:
        epics:
          rules:
            - name: Rule name
              conditions:
                state: opened
              actions:
                comment: |
                  This is my comment for \#{resource[:type]}
    YAML

    expect { perform(rule) }.to raise_error(Gitlab::Triage::Engine::EpicsTriagingForProjectImpossibleError)
  end
end
